import 'package:flutter/material.dart';


import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Quote(),
    ));
  }
}

class Quote extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getQuote(),
        builder: (context, snapshot) {
          return snapshot.connectionState == ConnectionState.done
              ? Center(
                  child: Text(
                  snapshot.data,
                  textAlign: TextAlign.center,
                ))
              : Center(child: CircularProgressIndicator());
        });
  }
}

/// Quote kindly supplied by https://theysaidso.com/api/
Future<String> _getQuote() async {
  final res = await http.get('http://quotes.rest/qod.json');
  return json.decode(res.body)['contents']['quotes'][0]['quote'];
}
/*
class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}
class MyAppState extends State<MyApp>  {
  var qustions = [
      "What\'s your favorite color?",
      "What\'s your favorite animal?",
    ];
    var questionIndex = 0;
  @override
  Widget build(BuildContext context) {


    
    void answerQuestion(){
      setState(() {
        questionIndex = questionIndex + 1;  
        
      });
    }

    
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("The First App"),
        ),
        body: Column(
          children: <Widget>[
            Text(qustions[questionIndex]),
            RaisedButton(
              child: Text('Answer 1'),
              onPressed: answerQuestion,
            ),
            RaisedButton(
              child: Text('Answer 2'),
              onPressed: answerQuestion,
            ),
            RaisedButton(
              child: Text('Answer 3'),
              onPressed: answerQuestion,
            ),
          ],
        ),
      ),
    );
  }
}

*/
